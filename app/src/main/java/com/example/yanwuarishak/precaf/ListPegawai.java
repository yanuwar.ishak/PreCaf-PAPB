package com.example.yanwuarishak.precaf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ListPegawai extends AppCompatActivity implements View.OnClickListener {

    private TextView Matthew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pegawai);
        Matthew = (TextView) findViewById(R.id.FranklinM);
        Matthew.setOnClickListener(this);
    }

    public void onClick (View detailmatthew)
    {
        switch (detailmatthew.getId())
        {
            case R.id.FranklinM:
                Intent moveIntent = new Intent(ListPegawai.this, DetailPegawai.class);
                startActivity(moveIntent);
                break;
        }
    }
}
