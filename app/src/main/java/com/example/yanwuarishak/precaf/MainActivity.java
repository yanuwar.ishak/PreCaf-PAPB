package com.example.yanwuarishak.precaf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnJaga;
    private TextView gantiTeksJaga;
    private boolean statusJaga = false;
    private CardView ManajemenPegawai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnJaga = (Button) findViewById(R.id.buttonJaga);
        gantiTeksJaga = (TextView) findViewById(R.id.txtStatus);

        btnJaga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!statusJaga) {
                    btnJaga.setText("SELESAI");
                    gantiTeksJaga.setText("Klik tombol diatas\njika berhenti jaga");
                    statusJaga = true;
                } else {
                    btnJaga.setText("JAGA");
                    gantiTeksJaga.setText("Klik tombol di atas\njika ingin mulai jaga");
                    statusJaga = false;
                }
            }
        });

        ManajemenPegawai = (CardView) findViewById(R.id.card_manajemen);
        ManajemenPegawai.setOnClickListener(this);
    }

    public void onClick (View viewmanajemen)
    {
        switch (viewmanajemen.getId())
        {
            case R.id. card_manajemen:
                Intent moveIntent = new Intent(MainActivity.this, ListPegawai.class);
                startActivity(moveIntent);
                break;
        }
    }
}
